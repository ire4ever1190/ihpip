### IHpip
simple extension to the pip -r command which allows you to separate your dependencies
for different environments e.g. testing and production

sample requirements.yml
```yaml
test:
  - pytest
  - tox

prod:
  - pyyaml
```
to install the test dependencies run
```ihpip requirements.yml test```

this can also be used to separate python dependencies for different docker containers
