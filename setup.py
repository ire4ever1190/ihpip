from setuptools import setup

try:
	with open("readme.md", "r") as fh:
		long_description = fh.read()
except:
	long_description = """
### IHpip
simple extension to the pip -r command which allows you to separate your dependencies
for different environments e.g. testing and production

sample requirements.yml
```yaml
test:
  - pytest
  - tox

prod:
  - pyyaml
```
to install the test dependencies run
```ihpip requirements.yml test```

this can also be used to separate python dependencies for different docker containers
	"""

setup(
	name='IHpip',
	version='0.1.0',
	packages=['IHpip'],
	url='https://gitlab.com/ire4ever1190/ihpip',
	license='',
	long_description=long_description,
	long_description_content_type="text/markdown",
	author='Jake Leahy',
	author_email='darhyaust@gmail.com',
	description='Small package that makes requirement.txt files better',
	entry_points={
		'console_scripts': [
			'ihpip = IHpip:run'
		]
	},
)
